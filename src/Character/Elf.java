package Character;

public class Elf extends Character {

    private String subRace;
    private int magicka;

    public String getSubRace() {
        return subRace;
    }

    public void setSubRace(String name) {
        this.subRace = subRace;
    }

    public int getMaxMagic() {
        return magicka;
    }

    public void setMaxMagic(int magicka) {
        this.magicka = magicka;
    }


}
