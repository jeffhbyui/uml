package Character;

public class CreateCharacter {

    public static Character buildDefaultCharacter() {

        Character defaultCharacter = new Character();
        defaultCharacter.setHp(40);
        defaultCharacter.setName("Joe");
        defaultCharacter.setRace("Human");
        defaultCharacter.setWeaponId(0);
        return defaultCharacter;
    }

    public static Elf buildDefaultElf() {

        Elf defaultCharacter = new Elf();
        defaultCharacter.setHp(40);
        defaultCharacter.setName("Joe");
        defaultCharacter.setRace("Human");
        defaultCharacter.setWeaponId(0);
        defaultCharacter.setMaxMagic(35);
        defaultCharacter.setSubRace("Wood Elf");
        return defaultCharacter;
    }

    public static Dwarf buildDefaultDwarf() {

        Dwarf defaultCharacter = new Dwarf();
        defaultCharacter.setHp(40);
        defaultCharacter.setName("Joe");
        defaultCharacter.setRace("Human");
        defaultCharacter.setWeaponId(0);
        defaultCharacter.setSecondWeaponId(0);
        defaultCharacter.setCanSing(false);
        return defaultCharacter;
    }
}
