package Character;

public class Dwarf extends Character {

    private int secondWeaponId;
    private boolean canSing;

    public int getSecondWeaponId() {
        return secondWeaponId;
    }

    public void setSecondWeaponId(int secondWeaponId) {
        this.secondWeaponId = secondWeaponId;
    }

    public boolean getCanSing() {
        return canSing;
    }

    public void setCanSing(boolean canSing) {
        this.canSing = canSing;
    }
}
